const {USER} = require('lit-constants')

const welcome = (lang) => {
  if (lang === USER.LANG.EN) {
    return 'Chuc mung ban da kich hoat thanh cong tai khoan \"Lit Now"\. Dang nhap & mua sam ngay ban nhe.'
  } else {
    return 'Chuc mung ban da kich hoat thanh cong tai khoan \"Lit Now"\. Dang nhap & mua sam ngay ban nhe.'
  }
}

const creditErrorAlert = (lang) => {
  if (lang === USER.LANG.EN) {
    return 'Tai khoan cua ban hien khong the khau tru tu dong va dang bi khoa. De tranh phi phat sinh, vui long nap them tien vao tai khoan hoac lien he 1900638336 (1000d/phut).'
  } else {
    return 'Tai khoan cua ban hien khong the khau tru tu dong va dang bi khoa. De tranh phi phat sinh, vui long nap them tien vao tai khoan hoac lien he 1900638336 (1000d/phut).'
  }
}

const creditAlert = (lang) =>{
  if (lang === USER.LANG.EN) {
    return 'De tranh bi lam phien vi loi nhac thanh toan, vui long nap tien vao tai khoan hoac cap nhat the moi. Lien he 1900638336 (1000d/phut).'
  } else {
    return 'De tranh bi lam phien vi loi nhac thanh toan, vui long nap tien vao tai khoan hoac cap nhat the moi. Lien he 1900638336 (1000d/phut).'
  }
}

const purchaseNotification = (orderId, date, deadline, lang) => {
  if (lang === USER.LANG.EN) {
    return `Nhac nho: Don hang ${orderId} den han thanh toan vao ngay ${date}, vui long thanh toan truoc ngay ${deadline}.`
  } else {
    return `Nhac nho: Don hang ${orderId} den han thanh toan vao ngay ${date}, vui long thanh toan truoc ngay ${deadline}.`
  }
}

const completeFail = (lang) =>{
  if (lang === USER.LANG.EN) {
    return 'Xin loi Quy Khach, chung toi rat tiec thong bao tai khoan "Lit Now" cua ban hien khong du dieu kien de kich hoat.'
  } else {
    return 'Xin loi Quy Khach, chung toi rat tiec thong bao tai khoan "Lit Now" cua ban hien khong du dieu kien de kich hoat.'
  }
}

module.exports = {
  welcome,
  creditAlert,
  creditErrorAlert,
  purchaseNotification,
  completeFail
}