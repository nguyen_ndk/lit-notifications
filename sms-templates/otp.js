const {USER, PINCODE} = require('lit-constants')

module.exports = (code, lang) => {
  if (lang === USER.LANG.EN) {
    return `Ma xac minh tai khoan LIT la ${code} co hieu luc trong ${PINCODE.EXPIRED_TIME} phut.`
  } else {
    return `Ma xac minh tai khoan LIT la ${code} co hieu luc trong ${PINCODE.EXPIRED_TIME} phut.`
  }
}