const _ = require('lodash')
const {facade: {SmsFacade, Log}} = require('lit-utils')
const {SmsLogRepo} = require('lit-repositories')
const otpTemplate = require('./sms-templates/otp')
const { welcome, completeFail } = require('./sms-templates/template')
class Sms {

  sendWelcome(user){
    this.send(user.phone, welcome(user.lang))
      .catch(e => {
        Log.error(`SEND_SMS_WELCOME USER ${user.id}`, e)
      })
  }

  sendOtp(phoneNumber, otp, lang) {
    let template = otpTemplate(otp, lang)
    return this.send(phoneNumber, template)
  }

  sendFailedKyc(user) {
    this.send(user.phone, completeFail(user.lang))
      .catch(e => {
        Log.error(`SEND_SMS_KYC_FAILED USER ${user.id}`, e)
      })
  }

  async send(phone, body) {
    let ctx = {}
    ctx.phoneNumber = phone
    ctx.message = body
    const response = await SmsFacade.send(phone, body)
    this.saveLog(ctx, response)
    return response
  }

  saveLog(ctx, response) {
    const saveData = {
      type: _.get(ctx, 'type'),
      phoneNumber: ctx.phoneNumber,
      message: ctx.message,
      userId: _.get(ctx, 'userId'),
      response: response.message,
      carrier: response.carrier
    }
    SmsLogRepo.create(saveData)
      .catch(e => {
        Log.error(`SAVE_SMS_LOG USER ${ctx.userId}`, e)
      })
  }
}


module.exports = new Sms()
