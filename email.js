const _ = require('lodash')
const {facade: {EmailFacade, Log}} = require('lit-utils')
const {BASE_URL, FRONT_END_BASE_URL, MERCHANT_FE_BASE_URL, DEVOPS_EMAILS, EMAIL} = require('lit-constants')
const verificationEmailTemplate = require('./email-templates/email-validation')
const resetPasswordTemplate = require('./email-templates/reset-password')
const autoBillingTemplate = require('./email-templates/auto-billing')
const successInstallmentTpl = require('./email-templates/sucess-installment')
const payAllNowTemplate = require('./email-templates/pay-all-now')
const newPurchaseConfirmTemplate = require('./email-templates/new-purchase-confirm')
const purchaseSucessUserTemplate = require('./email-templates/refund-success-user')
const purchaseSucessMerchantTemplate = require('./email-templates/refund-success-merchant')
const changeEmailTemplate = require('./email-templates/change-email')
const sms = require('./sms')
const installmentDueTpl = require('./email-templates/installment-due')
const lateFeeTemplate = require('./email-templates/late-fee')
const paymentSuccessTpl = require('./email-templates/payment-success')
const welcomeTemplate = require('./email-templates/wellcome')
const addCardTemplate = require('./email-templates/remind-add-card')
const invalidCardTemplate = require('./email-templates/invalid-bank-card')
const addPhoneTemplate = require('./email-templates/remind-add-phone')
const merchantCreateTpl = require('./email-templates/merchant-create-account')
const successOverallBillingTemplate = require('./email-templates/overall-billing/success')
const failedOverallBillingTemplate = require('./email-templates/overall-billing/failed')
const lateFeeWarningTemplate = require('./email-templates/late-fee/warning')
const merchantForgotPassword = require('./email-templates/merchant-forgot-password')



const defaultFrom = {
  name: EMAIL.SENDER_NAME,
  address: EMAIL.SENDER_ADDRESS
}

const sendVerificationEmail = async (token, email) => {
  try {
    const url = `${BASE_URL}/email-verification?token=${token}`
    const body =  verificationEmailTemplate(url)
    await EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Xác minh email', body)
  } catch (e) {
    Log.info('SENDING_VERIFICATION_EMAIL', email)
  }
}

const sendResetPasswordEmail = async (token, email) => {
  const url = `${FRONT_END_BASE_URL}/reset-password?token=${token}`
  const body =  resetPasswordTemplate(url)
  await EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Đổi mật khẩu', body)
}

const sendChangeEmailEmail = async (token, email) => {
  const url = `${BASE_URL}/UsVeNeEm?token=${token}`
  const body = changeEmailTemplate(url)
  await EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Đổi email', body)
}

const sendPhoneCode = (code, phone, ctx) => {
  return sms.sendOtp(phone, code, ctx).catch(e => {
    Log.error(`SEND_PINCODE user ${ctx.userId}`, {error: e})
  })
}

//Temporary send otp by email
const sendPhoneOtpToEmail = (code, email) => {
  EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Phone OTP by email', code)
}

const sendDevopsNotification = (notification) => {
  const emails = _.split(DEVOPS_EMAILS, ',')
  emails.forEach(email => {
    EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Daily Auto Billing Report', notification)
  })
}

const sendAutoBilling = (user, updated, totalAmount, merchantNameById) => {
  const body = autoBillingTemplate(user, updated, totalAmount, merchantNameById)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + 'New successful auto charged installment', body)
}

const sendSuccessOverallBilling = (user, purchaseInstallment) => {
  const body = successOverallBillingTemplate(purchaseInstallment)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + `✔✔ Thanh toán đợt ${purchaseInstallment.installmentNumber} thành công!`, body)
}

const sendFailedOverallBilling = (user) => {
  const body = failedOverallBillingTemplate(user)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + 'Rất tiếc - không thể thực hiện khấu trừ tự động!', body)
}

const sendSuccessInstallment = (email, credit, totalAmount, p) => {
  const body = successInstallmentTpl(credit, totalAmount, p)
  return EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Khoản trả góp mới thành công', body)
}
const sendFailedAutoBilling = (email, notification) => {
  EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Thanh toán tự động không thành công', notification)
}

const sendPurchaseCompleted = (email, credit, totalAmount, p) => {
  const body = payAllNowTemplate(credit, totalAmount, p)
  return EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}-` + 'Mua hàng đã hoàn thành', body)
}

const sendPaymentSuccess = (email, credit, totalAmount, p) => {
  const body = paymentSuccessTpl(credit, totalAmount, p)
  EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Thanh toán thành công', body)
    .catch(e => {
      Log.error(`SEND_PAYMENT_SUCCESS EMAIL ${email} PURCHASE ${p.id}`, e)
    })
}

const sendNewPurchase = (email, credit, deducted, purchase, merchant) => {
  const body = newPurchaseConfirmTemplate(credit, deducted, purchase, merchant)
  EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Thanh toán thành công', body)
    .catch(e => {
      Log.error(`SEND_NEW_PURCHASE user ${email}`, e)
    })
}

const sendFailedToRefundInstallment = (email, purchaseId, installmentId) => {
  const body = `Hoàn tiền thất bại cho thanh toán ${purchaseId} cho lần trả góp ${installmentId}`
  return EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Failed to refund installment', body)
}

const sendFailedToRefund = (email, purchaseId) => {
  const body = `Hoàn tiền thất bại cho thanh toán  ${purchaseId} `
  return EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Hoàn tiền thất bại', body)
}

const sendRefundSuccessForUser = (email, amount, credit, purchase, merchant) => {
  const body = purchaseSucessUserTemplate(amount, credit, purchase, merchant)
  return EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}-` + 'Hoàn tiền thành công', body)
}

const sendRefundSuccessForMerchant = (email, amount, purchase, user) => {
  const body = purchaseSucessMerchantTemplate(amount, purchase, user)
  return EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}-` + 'Hoàn tiền thành công', body)
}

const sendInstallmentDue = (purchase, installmentAmount, date) => {
  const body = installmentDueTpl(purchase, installmentAmount, date)
  return EmailFacade.send(defaultFrom, _.get(purchase, 'User.email'), `${EMAIL.PREFIX}` + 'Giao dịch của bạn đến hạn của bạn', body)
}

const sendLateFee = (user, lateFee) => {
  const body = lateFeeTemplate(lateFee)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + 'Một khoản phí trả chậm đã được thêm vào giao dịch mua của bạn', body)
}

//Send one day before apply latefee
const sendLateFeeWarning = (user, purchaseInstallment) => {
  const body = lateFeeWarningTemplate(user, purchaseInstallment)
  EmailFacade.send('noreply@litpay.vn', user.email, `${EMAIL.PREFIX}` + 'Tránh trễ hạn, trả liền mua tiếp nhé bạn ơi!', body)
}

const sendRemindAddCard = (user) => {
  const body = addCardTemplate(user.lastName)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + '💳 Bạn ơi, hãy nhớ nhập thẻ thanh toán nhé!', body)
}

const sendRemindAddPhone = (user) => {
  const body = addPhoneTemplate(user.lastName)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + '📲 Bạn ơi, hãy nhớ nhập SĐT nhé!', body)
}

const sendRemindInvalidBank = (user) => {
  const body = invalidCardTemplate(user.lastName)
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + 'Bạn ơi, hãy nhập thẻ mới!', body)
}

const sendWelcome = (user) => {
  const body = welcomeTemplate()
  EmailFacade.send(defaultFrom, user.email, `${EMAIL.PREFIX}` + '💫 Chào mừng bạn đến với LIT 💫', body).catch(e => {
    Log.error(`SEND_EMAIL_WELCOME USER ${user.id}`, e)
  })
}
const sendMerchantCreate = (email, token) => {
  const url = `${MERCHANT_FE_BASE_URL}/auth/registration?token=${token}`
  const body = merchantCreateTpl(email, url)
  EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'Tai khoan LIT Merchant', body)
}

const sendMerchantForgot = (email, token) => {
  const url = `${MERCHANT_FE_BASE_URL}/auth/forgot?token=${token}`
  const body = merchantForgotPassword(email, url)
  EmailFacade.send(defaultFrom, email, `${EMAIL.PREFIX}` + 'LIT Merchant - Đổi mật khẩu', body)
}
module.exports = {
  sendVerificationEmail,
  sendResetPasswordEmail,
  sendChangeEmailEmail,
  sendDevopsNotification,
  sendPhoneCode,
  sendAutoBilling,
  sendPurchaseCompleted,
  sendNewPurchase,
  sendFailedToRefundInstallment,
  sendFailedToRefund,
  sendFailedAutoBilling,
  sendSuccessInstallment,
  sendInstallmentDue,
  sendRefundSuccessForUser,
  sendRefundSuccessForMerchant,
  sendLateFee,
  sendPaymentSuccess,
  sendWelcome,
  sendRemindAddCard,
  sendRemindAddPhone,
  sendRemindInvalidBank,
  sendMerchantCreate,
  sendLateFeeWarning,
  sendSuccessOverallBilling,
  sendFailedOverallBilling,
  sendMerchantForgot,
  sendPhoneOtpToEmail
}