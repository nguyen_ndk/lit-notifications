module.exports = (url) => {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
  
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Email Template</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <style type="text/css">
      a[x-apple-data-detectors] {
          color: inherit !important;
      }
      </style>
  </head>
  
  <body style="margin: 0; padding: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
              <td style="padding: 0px 0 0px 0;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="750" style="border-collapse: collapse; border: 1px solid #cccccc;">
                      <tr>
                          <td align="center" bgcolor="#ffffff"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/banner_notif.png" alt="Banner Top" width="750" height="201" style="display: block;" />
                          </td>
                      </tr>
                      <tr>
                          <td bgcolor="#ffffff">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                  <tr>
                                      <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                              <tr>
                                                  <td style="color: #06098A; font-family: 'Quicksand', sans-serif;">
                                                      <h1 style="font-size: 24px; margin: 0;">Xác minh email</h1>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td style="color: #06098A; font-family: Arial, sans-serif; font-size: 16px; line-height: 24px; padding: 20px 0 30px 0;">
                                                      <p style="margin: 0;">Click <a href='${url}' target='_blank'>vào đây</a> để xác minh email của bạn!</p>
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td bgcolor="#0000AD" height="20">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                              <tr>
                                                  <td width="350" valign="top">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                                          <tr>
                                                              <td colspan="4" style="color: #153643; font-family: Arial, sans-serif; font-size: 14px; line-height: 24px;  padding: 20px 20px 20px 20px; text-align:center;">
                                                                  <p style="margin: 0;">Tổng đài: <a>1900 638 336</a>
                                                                  </p>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="padding: 1px 1px 1px 20px;"><a href="https://play.google.com/store/apps/details?id=com.lit.mobile"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/ps.png" border="0" alt="" /></a></td>
                                                              <td style="padding: 1px 1px 1px 1px;"><a href=" https://appgallery.cloud.huawei.com/ag/n/app/C104202589"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/ag.png" border="0" alt="" /></a></td>
                                                              <td style="padding: 1px 1px 1px 1px;"><a href="https://apps.apple.com/vn/app/lit-now/id1563192046"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/as.png" border="0" alt="" /></a></td>
                                                              <td style="padding: 1px 1px 1px 1px;"><a href="https://apps.samsung.com/appquery/appDetail.as?appId=com.lit.mobile"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/ss.png" border="0" alt="" /></a></td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                                  <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                                  <td width="350" valign="top">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                                          <tr>
                                                              <td style="color: #153643; font-family: Arial, sans-serif; font-size: 14px; line-height: 24px;  padding: 20px 20px 20px 20px; text-align:center;">
                                                                  <p style="margin: 0;">Email: <a href="mailto:help@litnow.vn">help@litnow.vn</a></p>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="padding: 1px 1px 10px 1px;text-align:center;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                                      <tr>
                                                                          <td><a href="https://www.litnow.vn"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/1.png" border="0" alt="" /></a></td>
                                                                          <td><a href="https://www.facebook.com/LIT.Life.in.a.Tap"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/2.png" border="0" alt="" /></a></td>
                                                                          <td><a href="https://www.instagram.com/litpay/"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/3.png" border="0" alt="" /></a></td>
                                                                          <td><a href="https://www.youtube.com/channel/UCOtTOPCPzctPJpAyeRXYvow"><img src="https://s3-ap-southeast-1.amazonaws.com/qr.litpay.vn/emailing/4.png" border="0" alt="" /></a></td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td bgcolor="#fa50ae" style="padding: 15px 15px;">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                                              <tr>
                                                  <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; text-align: center;">
                                                      <p style="margin: 0;">LIT Technology Co., Ltd, Tầng 10, Friendship Tower, 31 Lê Duẩn, P. Bến Nghé, Q.1, TP.HCM</p>
                                                      <p><img src="https://www.w3.org/Icons/valid-xhtml10-blue" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /><img src="https://sectigo.com/images/seals/sectigo_trust_seal_lg_2x.png" alt="Sectigo" height="31" width="88" /></p>
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
      </table>
  </body>
  
  </html>`
}